package com.blackfriday52.blackfriday52app.activites;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.apihelper.ApiClient;
import com.blackfriday52.blackfriday52app.apihelper.ApiInterface;
import com.blackfriday52.blackfriday52app.customview.CustomTextView;
import com.blackfriday52.blackfriday52app.model.details_response.MainObjectDetails;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDetails extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {
    private static final String TAG = ActivityDetails.class.getSimpleName();
    public static final String KEY = "key";
    @BindView(R.id.tvOwner)
    CustomTextView tvOwner;
    @BindView(R.id.tvUnit)
    CustomTextView tvUnit;
    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;
    @BindView(R.id.tvBusinessType)
    CustomTextView tvBusinessType;
    @BindView(R.id.tvServiceOffered)
    CustomTextView tvServiceOffered;
    @BindView(R.id.tvAddress)
    CustomTextView tvAddress;
    @BindView(R.id.tvKeyword)
    CustomTextView tvKeyword;
    @BindView(R.id.tvClose)
    CustomTextView tvClose;
    @BindView(R.id.tvDirectoryName)
    CustomTextView tvDirectoryName;

    @BindView(R.id.tvFax)
    CustomTextView tvFax;
    @BindView(R.id.tvWebsite)
    CustomTextView tvWebsite;
    @BindView(R.id.tvPhone)
    CustomTextView tvPhone;

    @BindView(R.id.tvWebsiteLabel)
    CustomTextView tvWebsiteLabel;
    @BindView(R.id.tvPhoneLabel)
    CustomTextView tvPhoneLabel;
    @BindView(R.id.tvFaxLabel)
    CustomTextView tvFaxLabel;


    @BindView(R.id.rlBackResult)
    RelativeLayout rlBackResult;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;

    //   @BindView(R.id.mapView)
    //   MapView mapView;

    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.transparent_image)
    ImageView transparent_image;
    @BindView(R.id.scrollViewMain)
    ScrollView scrollViewMain;

    ApiInterface apiInterface;
    String id;

    public static boolean isDeatilsActivity = false;

    String lat;
    String longs;
    String bdName;
    GoogleMap map;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detaiils_search);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        id = (String) getIntent().getSerializableExtra(KEY);
        ivMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDeatilsActivity = false;
                onBackPressed();
            }
        });
        rlBackResult.setOnClickListener(this);
        rlSearch.setOnClickListener(this);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        // mapFragment.getMapAsync(this);

        doGetDetails();

        loadTouch();


    }

    @SuppressLint("ClickableViewAccessibility")
    private void loadTouch() {
        transparent_image.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrollViewMain.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrollViewMain.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrollViewMain.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        if (!lat.equals("")) {
            Double mLat = Double.parseDouble(lat);
            Double mLong = Double.parseDouble(longs);


            LatLng sydney = new LatLng(mLat, mLong);

            map.addMarker(new MarkerOptions().position(sydney)
                    .title(bdName));
            map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sydney, 14);
            map.animateCamera(cameraUpdate);
        }

    }


    private void doGetDetails() {
        showDialog(this);

        String url = ApiClient.getClient().baseUrl() + "bd_details/" + id;
        Call<MainObjectDetails> responseCall = apiInterface.doGetDeatils(url);
        responseCall.enqueue(new Callback<MainObjectDetails>() {
            @Override
            public void onResponse(@NonNull Call<MainObjectDetails> call, @NonNull Response<MainObjectDetails> response) {

                Log.d(TAG, "onResponse: " + new Gson().toJson(response));
                BaseActivity.hideDialog();

                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getPostObjectDetails().getStatus().equals("Success")) {

                        String path = response.body().getPostObjectDetails().getDetailObjectDetails().getBd_image();
                        Glide.with(getApplicationContext()).load(path).into(ivImage);

                        String add = response.body().getPostObjectDetails().getDetailObjectDetails().getAddress() + ", " +
                                response.body().getPostObjectDetails().getDetailObjectDetails().getCity() + ", " +
                                response.body().getPostObjectDetails().getDetailObjectDetails().getState() + ", " +
                                response.body().getPostObjectDetails().getDetailObjectDetails().getZip();

                        bdName = response.body().getPostObjectDetails().getDetailObjectDetails().getBD_name();
                        tvDirectoryName.setText(bdName);
                        tvUnit.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getUnit_name());
                        tvEmail.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getEmail());
                        tvBusinessType.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getBusiness_type());
                        tvServiceOffered.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getServices_offered());
                        tvAddress.setText(add);
                        tvOwner.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getOwnerName());
                        tvClose.setText(response.body().getPostObjectDetails().getDetailObjectDetails().getClosed_Days());


                        if (response.body().getPostObjectDetails().
                                getDetailObjectDetails().getFax() != null) {
                            tvFax.setText(response.body().getPostObjectDetails().
                                    getDetailObjectDetails().getFax());
                        } else {

                            tvFax.setVisibility(View.GONE);
                            tvFaxLabel.setVisibility(View.GONE);

                        }
                        if (response.body().getPostObjectDetails().
                                getDetailObjectDetails().getWebsite() != null) {
                            tvWebsite.setText(response.body().getPostObjectDetails().
                                    getDetailObjectDetails().getWebsite());
                        } else {

                            tvWebsite.setVisibility(View.GONE);
                            tvWebsiteLabel.setVisibility(View.GONE);

                        }

                        if (response.body().getPostObjectDetails().
                                getDetailObjectDetails().getPhone() != null) {
                            tvPhone.setText(response.body().getPostObjectDetails().
                                    getDetailObjectDetails().getPhone());
                        } else {

                            tvPhone.setVisibility(View.GONE);
                            tvPhoneLabel.setVisibility(View.GONE);

                        }


                        lat = response.body().getPostObjectDetails().getDetailObjectDetails().getLatitude();
                        longs = response.body().getPostObjectDetails().getDetailObjectDetails().getLongitude();
                        mapFragment.getMapAsync(ActivityDetails.this);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MainObjectDetails> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlBackResult:
                isDeatilsActivity = false;
                onBackPressed();
                break;
            case R.id.rlSearch:
                isDeatilsActivity = true;
                onBackPressed();
                break;

        }


    }
}
