package com.blackfriday52.blackfriday52app.activites;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.blackfriday52.blackfriday52app.R;
import com.wang.avi.AVLoadingIndicatorView;

public class BaseActivity extends AppCompatActivity {

    //dialog
    private static Dialog dialog;
    private static AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //Todo Show progress dialog
    public void showDialog(Activity activity) {
        if (dialog == null) {
            dialog = new Dialog(activity);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_indicator);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            avi = dialog.findViewById(R.id.avi);
            avi.show();
            dialog.setCancelable(false);
            dialog.show();
        }

    }

    //Todo hide dialog
    public static void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
            avi.hide();
        }
    }


  /*  public void showAlertDialog(Activity activity, String title, String msg) {
        if (dialog == null) {
            dialog = new Dialog(BaseActivity.this);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_alert);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextView tvTitle = dialog.findViewById(R.id.tvTitle);
            CustomTextView tvMessage = dialog.findViewById(R.id.tvMessage);
            CustomButtonView bOk = dialog.findViewById(R.id.bOk);
            tvTitle.setText(title);
            tvMessage.setText(msg);

            bOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  dialog.dismiss();
                    hideDialog();
                    doneDialog();
                }
            });
            dialog.setCancelable(false);
            dialog.show();
        }

    }


    public void doneDialog() {

    }*/


}
