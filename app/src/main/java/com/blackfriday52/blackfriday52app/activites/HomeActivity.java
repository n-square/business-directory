package com.blackfriday52.blackfriday52app.activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.customview.CustomTextView;
import com.blackfriday52.blackfriday52app.fragments.FragmentHome;
import com.blackfriday52.blackfriday52app.fragments.FragmentNearBy;
import com.blackfriday52.blackfriday52app.fragments.FragmentSearch;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.annotations.SerializedName;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements View.OnClickListener {
    private static final int PERMISSION = 1;
    private static final String ACCESS_COARSE = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String TAG = HomeActivity.class.getSimpleName();


    @BindView(R.id.tvHome)
    CustomTextView tvHome;
    @BindView(R.id.tvSearch)
    CustomTextView tvSearch;
    @BindView(R.id.tvNearBy)
    CustomTextView tvNearBy;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            } else {
                Log.d(TAG, "onCreate: already permission is getting");
            }
        }
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        tvHome.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
        tvNearBy.setOnClickListener(this);
        ivMenu.setOnClickListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentHome()).commit();

    }

    @SuppressLint("RtlHardcoded")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                drawer_layout.openDrawer(Gravity.LEFT);
                break;
            case R.id.tvHome:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentHome()).commit();
                drawer_layout.closeDrawers();
                break;
            case R.id.tvSearch:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentSearch()).commit();
                FragmentSearch.isSearchKeyword = false;
                drawer_layout.closeDrawers();
                break;
            case R.id.tvNearBy:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentNearBy()).commit();
                drawer_layout.closeDrawers();
                break;
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityDetails.isDeatilsActivity) {
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentHome()).commit();
        }
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_COARSE}, PERMISSION);
    }

    private boolean checkPermission() {
        int acccess_coars = ContextCompat.checkSelfPermission(this, ACCESS_COARSE);
        return acccess_coars == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        doOnGPS();
    }

    @Override
    protected void onPause() {
        super.onPause();
        doOnGPS();
    }

    void doOnGPS() {

        if (!checkPermission()) {
            requestPermission();
        } else {

            final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                             Log.d(TAG, "doOnGPS: gps on");
            } else {
                GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API).build();
                googleApiClient.connect();

                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(10000);
                locationRequest.setFastestInterval(10000 / 2);

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                builder.setAlwaysShow(true);

                PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                //             getLastLocation();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the result
                                    // in onActivityResult().
                                    status.startResolutionForResult(HomeActivity.this, 12);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                break;
                        }
                    }
                });
            }
            //  getLastLocation();
        }

    }

}
