package com.blackfriday52.blackfriday52app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.activites.ActivityDetails;
import com.blackfriday52.blackfriday52app.customview.CustomTextView;
import com.blackfriday52.blackfriday52app.interfaces.OnItemClick;
import com.blackfriday52.blackfriday52app.model.search_response.DetailsSearch;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterSearch extends RecyclerView.Adapter<AdapterSearch.ViewHolder> {
    private Context context;
    private List<DetailsSearch> detailsSearchList = new ArrayList<>();
    private OnItemClick onItemClick;

    public void setItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public AdapterSearch(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //  String path = "http://blackfriday52.com/uploads/BD_images/" + detailsSearchList.get(position).getBd_image();
        String path = detailsSearchList.get(position).getBd_image();


        Glide.with(context).load(path).into(holder.ivImage);
        holder.tvDirectoryName.setText(detailsSearchList.get(position).getBD_name());
        holder.tvOwnerName.setText(detailsSearchList.get(position).getOwner_name());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return detailsSearchList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImage)
        ImageView ivImage;
        @BindView(R.id.tvDirectoryName)
        CustomTextView tvDirectoryName;
        @BindView(R.id.tvOwnerName)
        CustomTextView tvOwnerName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    onItemClick.onClickItem(detailsSearchList.get(pos).getBD_id());
                }
            });
        }
    }

    public void setList(List<DetailsSearch> detailsSearchList) {
        this.detailsSearchList = detailsSearchList;
        notifyDataSetChanged();
    }


    public void setClearList() {
        this.detailsSearchList.clear();
        notifyDataSetChanged();
    }


}
