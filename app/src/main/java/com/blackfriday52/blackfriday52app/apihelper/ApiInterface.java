package com.blackfriday52.blackfriday52app.apihelper;


import com.blackfriday52.blackfriday52app.model.ads_model.AdsMainResponse;
import com.blackfriday52.blackfriday52app.model.details_response.MainObjectDetails;
import com.blackfriday52.blackfriday52app.model.home_response.MainObject;
import com.blackfriday52.blackfriday52app.model.near_businees.MainObjectBusineess;
import com.blackfriday52.blackfriday52app.model.search_response.MainObjectSearch;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {


    //For login
    //@FormUrlEncoded
    @GET("Get_Home_data")
    Call<MainObject> doGetHomeData();

    //get all directories
    @GET("Get_All_BD")
    Call<MainObjectSearch> doGetSearchListAll();

    //get all ads
    @GET("Get_Ads")
    Call<AdsMainResponse> doAdsMainResponseCall();

    //get all directories using serach keyword
    @GET
    Call<MainObjectSearch> doGetSearchListAllSearch(@Url String url);

    // for details api
    @GET
    Call<MainObjectDetails> doGetDeatils(@Url String url);

    //for near by api
    @GET
    Call<MainObjectBusineess> doGetNearBussiness(@Url String url);

}
