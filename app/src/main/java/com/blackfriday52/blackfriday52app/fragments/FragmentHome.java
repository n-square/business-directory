package com.blackfriday52.blackfriday52app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.activites.BaseActivity;
import com.blackfriday52.blackfriday52app.activites.HomeActivity;
import com.blackfriday52.blackfriday52app.apihelper.ApiClient;
import com.blackfriday52.blackfriday52app.apihelper.ApiInterface;
import com.blackfriday52.blackfriday52app.customview.CustomEditTextView;
import com.blackfriday52.blackfriday52app.model.ads_model.AdsMainResponse;
import com.blackfriday52.blackfriday52app.model.home_response.MainObject;
import com.blackfriday52.blackfriday52app.utils.SharePref;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHome extends Fragment implements View.OnClickListener {
    private static final String TAG = FragmentHome.class.getSimpleName();
    View view;
    @BindView(R.id.etSearch)
    CustomEditTextView etSearch;
    @BindView(R.id.wvMessage)
    WebView wvMessage;
    @BindView(R.id.ivAds)
    ImageView ivAds;
    @BindView(R.id.ibSearch)
    ImageButton ibSearch;
    ApiInterface apiInterface;

    public static int size = 0;
    public static int pos = 0;

    List<String> stringList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        return view;
    }

    //initlastion   all veriable
    private void init() {
        ButterKnife.bind(this, view);
        ibSearch.setOnClickListener(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        wvMessage.getSettings();
        wvMessage.setBackgroundColor(Color.TRANSPARENT);
        wvMessage.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        doGetHomeDetails();
        doGetAllAds();


    }


    //call api for home details response
    private void doGetHomeDetails() {
        SharePref.showDialog(getActivity());
        Call<MainObject> responseCall = apiInterface.doGetHomeData();
        responseCall.enqueue(new Callback<MainObject>() {
            @Override
            public void onResponse(@NonNull Call<MainObject> call, @NonNull Response<MainObject> response) {
                SharePref.hideDialog();

                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getPostObject().getStatus().equals("Success")) {
                        String summary = response.body().getPostObject().getDetails().get(0).getLong_description();
                        wvMessage.loadData(summary, "text/html", null);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<MainObject> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                //     BaseActivity.hideDialog();
                SharePref.hideDialog();

            }
        });


    }

    //handle all types of click
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibSearch:
                SharePref.setSearchKeyword(getActivity(), etSearch.getText().toString().trim());
                FragmentSearch.isSearchKeyword = true;
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentSearch()).commit();
                break;
        }

    }


    //call api for Ads response
    private void doGetAllAds() {

        Log.d(TAG, "doGetAllAds: ");
        //   ((HomeActivity) getActivity()).showDialog(getActivity());
        SharePref.showDialog(getActivity());

        // showDialog(this);
        Call<AdsMainResponse> responseCall = apiInterface.doAdsMainResponseCall();
        responseCall.enqueue(new Callback<AdsMainResponse>() {
            @Override
            public void onResponse(@NonNull Call<AdsMainResponse> call, @NonNull Response<AdsMainResponse> response) {
                //   Log.d(TAG, "onResponse:     " + response.body().getPostObject().getStatus());
                Log.d(TAG, "ads onResponse:     " + new Gson().toJson(response));

                //   BaseActivity.hideDialog();
                SharePref.hideDialog();

                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getAdsPostModel().getStatus().equals("Success")) {
                        stringList = response.body().getAdsPostModel().getAds();
                        String url = stringList.get(pos);
                        Log.d(TAG, "ads single: " + url);
                        Glide.with(getActivity()).load(url).into(ivAds);
                    } else {
                        ivAds.setImageDrawable(getResources().getDrawable(R.drawable.add_image));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AdsMainResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                //  BaseActivity.hideDialog();
                SharePref.hideDialog();

            }
        });


    }

    //create login for next image
    @Override
    public void onDestroy() {
        super.onDestroy();
        size = size + 1;
        pos = pos + 1;

        if (stringList.size() > 0) {

            if (stringList.size() == size) {
                size = 0;
                pos = 0;

            }

        }
    }


}

