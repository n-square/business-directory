package com.blackfriday52.blackfriday52app.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.activites.ActivityDetails;
import com.blackfriday52.blackfriday52app.activites.BaseActivity;
import com.blackfriday52.blackfriday52app.activites.HomeActivity;
import com.blackfriday52.blackfriday52app.adapters.AdapterNearBy;
import com.blackfriday52.blackfriday52app.adapters.AdapterSearch;
import com.blackfriday52.blackfriday52app.apihelper.ApiClient;
import com.blackfriday52.blackfriday52app.apihelper.ApiInterface;
import com.blackfriday52.blackfriday52app.customview.CustomEditTextView;
import com.blackfriday52.blackfriday52app.interfaces.OnItemClick;
import com.blackfriday52.blackfriday52app.model.near_businees.MainObjectBusineess;
import com.blackfriday52.blackfriday52app.utils.SharePref;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.wang.avi.AVLoadingIndicatorView;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNearBy extends Fragment implements OnItemClick {
    private static final String TAG = FragmentNearBy.class.getSimpleName();
    private static final int PERMISSION = 1;
    private static final String ACCESS_COARSE = Manifest.permission.ACCESS_COARSE_LOCATION;

    View view;
    @BindView(R.id.rlData)
    RecyclerView rlData;

    AdapterNearBy adapterSearch;
    ApiInterface apiInterface;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    Location mCurrentLocation;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_near_business, container, false);
        init();
        return view;
    }

    private void init() {
        ButterKnife.bind(this, view);
        rlData.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapterSearch = new AdapterNearBy(getActivity());
        adapterSearch.setItemClick(this);
        rlData.setAdapter(adapterSearch);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        //  mSettingsClient = LocationServices.getSettingsClient(getActivity());
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();


        /// displayLocationSettingsRequest(getContext());

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermission()) {
            requestPermission();
        } else {

            final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // Intent mServiceIntent = new Intent(getApplicationContext(), CurrentLocationServices.class);
                // startService(mServiceIntent);
                getLastLocation();
            } else {
                displayLocationSettingsRequest();
            }
            //  getLastLocation();
        }

    }


    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        // mCurrentLocation =
                        if (task.isSuccessful() && task.getResult() != null) {
                            mCurrentLocation = task.getResult();
                            String latLong = mCurrentLocation.getLatitude() + "/" + mCurrentLocation.getLongitude();
                            //  Log.d(TAG, "onComplete: ");


                            doGetAllBusinessList(latLong);


                        } else {
                            // Log.w(TAG, "getLastLocation:exception", task.getException());
                            //    Toast.makeText(getActivity(), "No Location detect", Toast.LENGTH_SHORT).show();
                            showAlert("Turn on GPS first to view Nearby Businesses");
                            //  displayLocationSettingsRequest();

                        }
                    }
                });
    }


    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_COARSE}, PERMISSION);
    }

    private boolean checkPermission() {
        int acccess_coars = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE);
        return acccess_coars == PackageManager.PERMISSION_GRANTED;
    }


    private void doGetAllBusinessList(String keyword) {

        if (getActivity() != null) {
            ((HomeActivity) getActivity()).showDialog(getActivity());
        }

        String url = ApiClient.getClient().baseUrl() + "get_nearby_bd/" + keyword;
        Call<MainObjectBusineess> responseCall = apiInterface.doGetNearBussiness(url);
        responseCall.enqueue(new Callback<MainObjectBusineess>() {
            @Override
            public void onResponse(@NonNull Call<MainObjectBusineess> call, @NonNull Response<MainObjectBusineess> response) {
                BaseActivity.hideDialog();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getPostObjectBusiness().getStatus().equals("Success")) {
                        adapterSearch.setClearList();
                        adapterSearch.setList(response.body().getPostObjectBusiness().getDetails());
                    } else {
                        //  showAlert(response.body().getPostObjectBusiness().get);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MainObjectBusineess> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                // SharePref.hideDialog();
                BaseActivity.hideDialog();

            }
        });


    }

    private void showAlert(String alert) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("");
        alertDialogBuilder.setMessage(alert);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialogBuilder.create().show();

    }

    @Override
    public void onClickItem(String id) {
        //Log.d(TAG, "onClickItem  : " + id);
        Intent intent = new Intent(getContext(), ActivityDetails.class);
        intent.putExtra(ActivityDetails.KEY, id);
        ActivityDetails.isDeatilsActivity = false;
        startActivity(intent);
    }


    private void displayLocationSettingsRequest() {

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getLastLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), 12);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }


}
