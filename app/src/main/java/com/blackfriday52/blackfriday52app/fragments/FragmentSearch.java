package com.blackfriday52.blackfriday52app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.activites.ActivityDetails;
import com.blackfriday52.blackfriday52app.activites.BaseActivity;
import com.blackfriday52.blackfriday52app.activites.HomeActivity;
import com.blackfriday52.blackfriday52app.adapters.AdapterSearch;
import com.blackfriday52.blackfriday52app.apihelper.ApiClient;
import com.blackfriday52.blackfriday52app.apihelper.ApiInterface;
import com.blackfriday52.blackfriday52app.customview.CustomEditTextView;
import com.blackfriday52.blackfriday52app.interfaces.OnItemClick;
import com.blackfriday52.blackfriday52app.model.search_response.MainObjectSearch;
import com.blackfriday52.blackfriday52app.utils.SharePref;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSearch extends Fragment implements View.OnClickListener, OnItemClick {
    private static final String TAG = FragmentSearch.class.getSimpleName();
    private static final int PERMISSION = 1;
    private static final String ACCESS_COARSE = Manifest.permission.ACCESS_COARSE_LOCATION;

    View view;
    @BindView(R.id.etSearch)
    CustomEditTextView etSearch;
    @BindView(R.id.ibSearch)
    ImageButton ibSearch;
    @BindView(R.id.ibClear)
    ImageButton ibClear;

    @BindView(R.id.rlData)
    RecyclerView rlData;
    AdapterSearch adapterSearch;
    ApiInterface apiInterface;
    public static boolean isSearchKeyword = false;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    Location mCurrentLocation;
    String latLong;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        init();
        return view;
    }

    private void init() {
        ButterKnife.bind(this, view);
        rlData.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapterSearch = new AdapterSearch(getActivity());
        adapterSearch.setItemClick(this);
        rlData.setAdapter(adapterSearch);
        ibSearch.setOnClickListener(this);
        ibClear.setOnClickListener(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

        if (FragmentSearch.isSearchKeyword) {
            if (SharePref.getSearchKeyword(getActivity()).equals("")) {
                doGetAllSerachList();
            } else {
                String key = SharePref.getSearchKeyword(getActivity());
                etSearch.setText(key);
            }
        } else {
            doGetAllSerachList();
        }
    }

    private void doGetAllSerachList() {
        SharePref.showDialog(getActivity());
        Call<MainObjectSearch> responseCall = apiInterface.doGetSearchListAll();
        responseCall.enqueue(new Callback<MainObjectSearch>() {
            @Override
            public void onResponse(@NonNull Call<MainObjectSearch> call, @NonNull Response<MainObjectSearch> response) {
                SharePref.hideDialog();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getPostObjectSearch().getStatus().equals("Success")) {
                        adapterSearch.setList(response.body().getPostObjectSearch().getDetailsSearchList());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<MainObjectSearch> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                SharePref.hideDialog();
            }
        });


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ibSearch:
                if (!etSearch.getText().toString().equalsIgnoreCase("")) {
                    doGetAllSearchListFilter(etSearch.getText().toString().trim(), latLong);
                } else {

                    etSearch.setError("Enter Keyword");
                }
                break;

            case R.id.ibClear:
                etSearch.setText("");
                adapterSearch.setClearList();
                break;
        }

    }

    private void doGetAllSearchListFilter(String keyword, String latLong) {
        SharePref.showDialog(getActivity());
        String url = ApiClient.getClient().baseUrl() + "search_bd_nearby/" + keyword + "/" + latLong;
        Call<MainObjectSearch> responseCall = apiInterface.doGetSearchListAllSearch(url);
        responseCall.enqueue(new Callback<MainObjectSearch>() {
            @Override
            public void onResponse(@NonNull Call<MainObjectSearch> call, @NonNull Response<MainObjectSearch> response) {
                SharePref.hideDialog();

                if (response.code() == 200) {
                    if (getContext() != null) {

                        if (SharePref.getSearchKeyword(getContext()) != null) {
                            SharePref.setSearchKeyword(getContext(), "");

                        }
                    }
                    assert response.body() != null;
                    if (response.body().getPostObjectSearch().getStatus().equals("Success")) {
                        adapterSearch.setClearList();
                        adapterSearch.setList(response.body().getPostObjectSearch().getDetailsSearchList());
                    } else {
                        showAlert(response.body().getPostObjectSearch().getDetailsSearchList().get(0).getMsg());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MainObjectSearch> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                SharePref.hideDialog();
            }
        });


    }

    //for alert dialog
    private void showAlert(String alert) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("");
        alertDialogBuilder.setMessage(alert);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialogBuilder.create().show();

    }

    //item click of list
    @Override
    public void onClickItem(String id) {
        Intent intent = new Intent(getContext(), ActivityDetails.class);
        intent.putExtra(ActivityDetails.KEY, id);
        ActivityDetails.isDeatilsActivity = false;
        startActivity(intent);
    }

    //open permission dialog
    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_COARSE}, PERMISSION);
    }

    //check permision
    private boolean checkPermission() {
        int acccess_coars = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE);
        return acccess_coars == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onStart() {
        super.onStart();
        doOnGPS();
    }

    //for gps on with check permission
    void doOnGPS() {

        if (!checkPermission()) {
            requestPermission();
        } else {

            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                getLastLocation();
            } else {
                GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getActivity())
                        .addApi(LocationServices.API).build();
                googleApiClient.connect();

                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(10000);
                locationRequest.setFastestInterval(10000 / 2);

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                builder.setAlwaysShow(true);

                PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(getActivity(), 12);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                break;
                        }
                    }
                });
            }
            //  getLastLocation();
        }


    }


    //for get current location
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        // mCurrentLocation =
                        if (task.isSuccessful() && task.getResult() != null) {
                            mCurrentLocation = task.getResult();
                            latLong = mCurrentLocation.getLatitude() + "/" + mCurrentLocation.getLongitude();
                            //  Log.d(TAG, "onComplete: ");


                            Log.d(TAG, "onComplete:   " + latLong);

                            //     doGetAllBusinessList(latLong);

                            String key = etSearch.getText().toString().trim();
                            doGetAllSearchListFilter(key, latLong);
                        } else {
                            // Log.w(TAG, "getLastLocation:exception", task.getException());
                            //    Toast.makeText(getActivity(), "No Location detect", Toast.LENGTH_SHORT).show();
                            showAlert("Turn on GPS first to view Nearby Businesses");
                            //  displayLocationSettingsRequest();

                        }
                    }
                });
    }


}
