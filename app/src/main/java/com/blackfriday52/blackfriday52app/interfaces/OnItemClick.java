package com.blackfriday52.blackfriday52app.interfaces;

public interface OnItemClick {
    void onClickItem(String id);
}
