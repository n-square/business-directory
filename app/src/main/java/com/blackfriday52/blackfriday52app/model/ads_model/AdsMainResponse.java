package com.blackfriday52.blackfriday52app.model.ads_model;

import com.google.gson.annotations.SerializedName;

public class AdsMainResponse {
    @SerializedName("posts")
   private AdsPostModel adsPostModel;

    public AdsPostModel getAdsPostModel() {
        return adsPostModel;
    }

    public void setAdsPostModel(AdsPostModel adsPostModel) {
        this.adsPostModel = adsPostModel;
    }
}
