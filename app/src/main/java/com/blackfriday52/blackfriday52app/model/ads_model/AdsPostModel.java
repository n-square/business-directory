package com.blackfriday52.blackfriday52app.model.ads_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdsPostModel {

    @SerializedName("status")
    private String status;//": "Success",

    @SerializedName("ads")
    private List<String> ads;//":["http:\/\/blackfriday52.com\/uploads\/advertisement_images\/advertisement_20180802125733_5642.jpg","http:\/\/blackfriday52.com\/uploads\/advertisement_images\/advertisement_20180802125701_3174.jpg"]

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getAds() {
        return ads;
    }

    public void setAds(List<String> ads) {
        this.ads = ads;
    }
}
