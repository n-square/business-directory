package com.blackfriday52.blackfriday52app.model.details_response;

import com.google.gson.annotations.SerializedName;

public class DetailObjectDetails {


    @SerializedName("BD_id")
    private String BD_id;//": "2",
    @SerializedName("BD_name")
    private String BD_name;//": "Chinese Restaurant",
    @SerializedName("bd_image")
    private String bd_image;//": "http://blackfriday52.com/uploads/BD_images/BD_images_20180627192824_8466.jpg",
    @SerializedName("unit_name")
    private String unit_name;//": "Unit1",
    @SerializedName("subscription_date")
    private String subscription_date;//": "2018-05-17",
    @SerializedName("subscription_end_date")
    private String subscription_end_date;//": "2019-05-31",
    @SerializedName("business_type")
    private String business_type;//": "Tuitions",
    @SerializedName("services_offered")
    private String services_offered;//": "Online, Home, At My Home",
    @SerializedName("email")
    private String email;//": "wdw1@BD.com",
    @SerializedName("address")
    private String address;//": "test address",
    @SerializedName("city")
    private String city;//": "Atlanta",
    @SerializedName("state")
    private String state;//": "Georgia",
    @SerializedName("zip")
    private String zip;//": "12345654",
    @SerializedName("Closed_Days")
    private String Closed_Days;//": "10:00 AM to 7:00 PM"
    @SerializedName("latitude")
    private String latitude;//": "20.0159175",
    @SerializedName("longitude")
    private String longitude;//": "73.7670523"

    @SerializedName("owner_name")
    private String ownerName;//": "73.7670523"

    @SerializedName("website")
    private String website;//": null,
    @SerializedName("phone")
    private String phone;//": null,
    @SerializedName("fax")
    private String fax;//": null,


    public String getBD_id() {
        return BD_id;
    }

    public void setBD_id(String BD_id) {
        this.BD_id = BD_id;
    }

    public String getBD_name() {
        return BD_name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setBD_name(String BD_name) {
        this.BD_name = BD_name;
    }

    public String getBd_image() {
        return bd_image;
    }

    public void setBd_image(String bd_image) {
        this.bd_image = bd_image;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getSubscription_date() {
        return subscription_date;
    }

    public void setSubscription_date(String subscription_date) {
        this.subscription_date = subscription_date;
    }

    public String getSubscription_end_date() {
        return subscription_end_date;
    }

    public void setSubscription_end_date(String subscription_end_date) {
        this.subscription_end_date = subscription_end_date;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getServices_offered() {
        return services_offered;
    }

    public void setServices_offered(String services_offered) {
        this.services_offered = services_offered;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getClosed_Days() {
        return Closed_Days;
    }

    public void setClosed_Days(String closed_Days) {
        Closed_Days = closed_Days;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
