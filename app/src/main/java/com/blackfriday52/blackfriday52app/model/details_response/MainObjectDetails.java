package com.blackfriday52.blackfriday52app.model.details_response;

import com.google.gson.annotations.SerializedName;

public class MainObjectDetails {

    @SerializedName("posts")
    private PostObjectDetails postObjectDetails;

    public PostObjectDetails getPostObjectDetails() {
        return postObjectDetails;
    }

    public void setPostObjectDetails(PostObjectDetails postObjectDetails) {
        this.postObjectDetails = postObjectDetails;
    }
}
