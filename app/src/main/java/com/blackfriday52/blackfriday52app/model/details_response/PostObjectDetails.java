package com.blackfriday52.blackfriday52app.model.details_response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostObjectDetails {

    @SerializedName("status")
    private String status;//l": "Success",

    @SerializedName("details")
    private DetailObjectDetails detailObjectDetails;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailObjectDetails getDetailObjectDetails() {
        return detailObjectDetails;
    }

    public void setDetailObjectDetails(DetailObjectDetails detailObjectDetails) {
        this.detailObjectDetails = detailObjectDetails;
    }
}
