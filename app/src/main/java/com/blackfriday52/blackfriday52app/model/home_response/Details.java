package com.blackfriday52.blackfriday52app.model.home_response;

import com.google.gson.annotations.SerializedName;

public  class Details {

    @SerializedName("id")
    private String id;//": "1",

    @SerializedName("long_description")
    private String long_description;//": "1",

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }
}
