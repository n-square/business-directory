package com.blackfriday52.blackfriday52app.model.home_response;

import com.google.gson.annotations.SerializedName;

public class MainObject {

    @SerializedName("posts")
    private PostObject postObject;

    public PostObject getPostObject() {
        return postObject;
    }

    public void setPostObject(PostObject postObject) {
        this.postObject = postObject;
    }
}
