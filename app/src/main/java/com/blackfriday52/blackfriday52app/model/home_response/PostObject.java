package com.blackfriday52.blackfriday52app.model.home_response;

import com.blackfriday52.blackfriday52app.model.search_response.DetailsSearch;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostObject {
    @SerializedName("status")
    private String status;//": "Success",
    @SerializedName("details")
    private List<Details> details;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Details> getDetails() {
        return details;
    }

    public void setDetails(List<Details> details) {
        this.details = details;
    }
}
