package com.blackfriday52.blackfriday52app.model.near_businees;

import com.google.gson.annotations.SerializedName;

public class DetailsBusiness {
    @SerializedName("BD_id")
    private String BD_id;//": "26",
    @SerializedName("BD_name")
    private String BD_name;//": "Gangapur Road WDW",
    @SerializedName("bd_image")
    private String bd_image;//": "http://blackfriday52.com/assets/front/images/no-image3.jpg",
    @SerializedName("owner_name")
    private String owner_name;//": "SAM",
    @SerializedName("Distance")
    private String Distance;//": "0",
    @SerializedName("latitude")
    private String latitude;//": "20.0085486",
    @SerializedName("longitude")
    private String longitude;//": "73.745542"
    @SerializedName("msg")
    private String msg;//": "No Business Directories Available"

    public String getBD_id() {
        return BD_id;
    }

    public void setBD_id(String BD_id) {
        this.BD_id = BD_id;
    }

    public String getBD_name() {
        return BD_name;
    }

    public void setBD_name(String BD_name) {
        this.BD_name = BD_name;
    }

    public String getBd_image() {
        return bd_image;
    }

    public void setBd_image(String bd_image) {
        this.bd_image = bd_image;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
