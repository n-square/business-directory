package com.blackfriday52.blackfriday52app.model.near_businees;

import com.blackfriday52.blackfriday52app.model.home_response.PostObject;
import com.google.gson.annotations.SerializedName;

public class MainObjectBusineess {

    @SerializedName("posts")
    private PostObjectBusiness postObjectBusiness;


    public PostObjectBusiness getPostObjectBusiness() {
        return postObjectBusiness;
    }

    public void setPostObjectBusiness(PostObjectBusiness postObjectBusiness) {
        this.postObjectBusiness = postObjectBusiness;
    }
}
