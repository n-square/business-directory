package com.blackfriday52.blackfriday52app.model.near_businees;

import com.blackfriday52.blackfriday52app.model.home_response.Details;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostObjectBusiness {
    @SerializedName("status")
    private String status;//": "Success",
    @SerializedName("details")
    private List<DetailsBusiness> details;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DetailsBusiness> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBusiness> details) {
        this.details = details;
    }
}
