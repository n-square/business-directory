package com.blackfriday52.blackfriday52app.model.search_response;

import com.google.gson.annotations.SerializedName;

public class DetailsSearch {
    @SerializedName("BD_id")
    private String BD_id;//": "1",
    @SerializedName("BD_name")
    private String BD_name;//": "Plumber",
    @SerializedName("bd_image")
    private String bd_image;//": "BD_images_20180627192520_8503.jpg",
    @SerializedName("owner_name")
    private String owner_name;//": "Mr. Sandip J"

    @SerializedName("msg")
    private String msg;//": "No Business Directories Available"

    public String getBD_id() {
        return BD_id;
    }

    public void setBD_id(String BD_id) {
        this.BD_id = BD_id;
    }

    public String getBD_name() {
        return BD_name;
    }

    public void setBD_name(String BD_name) {
        this.BD_name = BD_name;
    }

    public String getBd_image() {
        return bd_image;
    }

    public void setBd_image(String bd_image) {
        this.bd_image = bd_image;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
