package com.blackfriday52.blackfriday52app.model.search_response;

import com.blackfriday52.blackfriday52app.model.home_response.PostObject;
import com.google.gson.annotations.SerializedName;

public class MainObjectSearch {

    @SerializedName("posts")
    private PostObjectSearch PostObjectSearch;


    public PostObjectSearch getPostObjectSearch() {
        return PostObjectSearch;
    }

    public void setPostObjectSearch(com.blackfriday52.blackfriday52app.model.search_response.PostObjectSearch postObjectSearch) {
        PostObjectSearch = postObjectSearch;
    }
}
