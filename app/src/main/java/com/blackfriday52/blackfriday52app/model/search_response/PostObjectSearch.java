package com.blackfriday52.blackfriday52app.model.search_response;

import com.blackfriday52.blackfriday52app.model.home_response.Details;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostObjectSearch {
    @SerializedName("status")
    private String status;//": "Success",
    @SerializedName("details")
    private List<DetailsSearch> detailsSearchList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DetailsSearch> getDetailsSearchList() {
        return detailsSearchList;
    }

    public void setDetailsSearchList(List<DetailsSearch> detailsSearchList) {
        this.detailsSearchList = detailsSearchList;
    }
}
