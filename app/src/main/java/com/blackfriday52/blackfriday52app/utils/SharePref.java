package com.blackfriday52.blackfriday52app.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.blackfriday52.blackfriday52app.R;
import com.blackfriday52.blackfriday52app.activites.BaseActivity;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by ALPHA-08 on 4/21/2017.
 */

public class SharePref {


    private static final String FILE_NAME = "cognegix";
    private static final String SEARCH_KEYWORD = "searchKeyword";
    //dialog
    private static Dialog dialog;
    private static AVLoadingIndicatorView avi;


    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }


    public static String getSearchKeyword(Context context) {
        return getPreferences(context).getString(SEARCH_KEYWORD, null);
    }


    public static boolean setSearchKeyword(Context context, String searchKeyword) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(SEARCH_KEYWORD, searchKeyword);
        return editor.commit();
    }

    //Todo Show progress dialog
    public static void showDialog(Activity activity) {
        if (dialog == null) {
            dialog = new Dialog(activity);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_indicator);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            avi = dialog.findViewById(R.id.avi);
            avi.show();
            dialog.setCancelable(false);
            dialog.show();
        }

    }

    //Todo hide dialog
    public static void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
            avi.hide();
        }
    }


}
